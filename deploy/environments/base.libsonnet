// this file has the baseline default parameters
{
  components: {
    graduate: {
      name: 'graduate',
      image: 'docker.io/graduatework/graduate',
      image_tag: 'latest',
      containerPort: 80,
      servicePort: 80,
      url: 'app.redlinx.ru',
      replicas: 1,
    },
  },
}

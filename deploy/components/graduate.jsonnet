local p = import '../params.libsonnet';
local params = p.components.graduate;

local makeTag = function (imageTag) (
  local ext_imageTag = std.extVar('qbec.io/tag');
  if ext_imageTag == '' then imageTag else ext_imageTag
);

[
// ========================== NAMESPACE =============================
  {
      apiVersion: 'v1',
      kind: 'Namespace',
      metadata: {
          name: std.extVar('qbec.io/defaultNs'),
      },
  },
// ========================== DEPLOYMENT =============================
  {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: params.name,
      labels: {
        app: params.name,
      },
    },
    spec: {
      replicas: params.replicas,
      selector: {
        matchLabels: {
          app: params.name,
        },
      },
      template: {
        metadata: {
          labels: {
            app: params.name,
          },
        },
        spec: {
          containers: [
            {
              name: 'website',
              image: params.image + ':' + makeTag(params.image_tag),
              imagePullPolicy: 'Always',

              ports: [
                {
                  containerPort: params.containerPort,
                },
              ],
            },
          ],
        },
      },
    },
  },

// ============================ SERVICE ===============================
  {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: "service-" + params.name,
    },
    spec: {
      selector: {
        app: params.name,
      },
      ports: [
        {
          port: params.servicePort,
        },
      ],
    },
  },

// ============================= INGRESS ===============================
  {
    "apiVersion": "networking.k8s.io/v1",
    "kind": "Ingress",
    "metadata": {
      "name": "ingress-" + params.name,
    },
    "spec": {
      "rules": [
        {
          "host": params.url,
          "http": {
            "paths": [
              {
                "path": "/",
                "pathType": "Prefix",
                "backend": {
                  "service": {
                    "name": "service-" + params.name,
                    "port": {
                      "number": params.servicePort,
                    },
                  },
                },
              },
            ],
          },
        },
      ],
    },
  },
]